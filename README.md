# assaporami-2.0

## premessa
Nuovo sito in sostituzione del sito Magento e-commerce e volto a presentare
la nuova offerta per catering e altri servizi a prevalenza ma non esclusività
B2B.

La business strategy si articola su tre linee di business:
- catering
- delivery
- assaporami@pranzo (pranzo in azienda)

e su due customer group:
- aziende
- privati

## release history

### 1.0 e precedenti

creazione dell'infrastruttura del sito con articolazione su tre pagine principali:
- index
- aziende
- privati

e due pagine secondarie:
- note legali
- contatti

integrazione di servizi terzi:
- Google Tag Manager
- Google Analytcs
- Zoho SalesIQ
- CMP


